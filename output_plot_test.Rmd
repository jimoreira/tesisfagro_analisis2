---
title: "plot_test"
author: "Javier Moreira"
date: '2024-04-24'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#graficas con hyperparametros segun modelo.
```{r warning=FALSE}

library(ggplot2)
library(patchwork)
library(cowplot)

# Assuming you have already fitted the models and stored them in a list
model_names <- c("fit_stefa","fit_nospt", "fit_colrow", "FitSTSPDE")
#models <- list(fit_nospt, fit_colrow, FitSTSPDE, fit_stefa)

# Define parameters you want to plot
parameters_to_plot <- c("Precision for the Gaussian observations", "Precision for IDbqe_x_amb_x_trat","Precision for IDbq_x_amb", "Precision for row", "Precision for col", "Rho for row", "GroupRho for row","Theta1 for FieldI","Theta2 for FieldI" )

# Function to extract summary statistics for a specific parameter
extract_summary <- function(model, parameter) {
  marginals_hyperpar <- get(model)$summary.hyperpar #%>% 
  if (parameter %in% row.names(marginals_hyperpar)) {
    filtrado <- marginals_hyperpar %>%
      filter(row.names(marginals_hyperpar) == parameter)
    row.names(filtrado) <- model
    return(filtrado)
  } else {
    # Create a data frame with NA values if parameter not found
    na_df <- data.frame(mean = NA, sd = NA, `0.025quant` = NA, `0.5quant` = NA, `0.975quant` = NA, mode = NA)
    colnames(na_df) <- c("mean", "sd", "0.025quant", "0.5quant", "0.975quant", "mode")
    
    row.names(na_df) <- model
    return(na_df)
  }
}


##cambiar de precision a varianza
reciprocal <- function(x, parameter) {
  if (parameter %in% c("Rho for row", "GroupRho for row", "Theta1 for FieldI", "Theta2 for FieldI")) {
    return(x)  # Return x without modification for the specified exceptions
  } else {
    if(is.numeric(x)) {
      return(1/x)
    } else {
      return(x)
    }
  }
}
# Plot posterior densities for each parameter across models
# Create an empty list to store plots
plot_list <- list()

# Set theme options for larger plot elements
theme_options <- theme(
  text = element_text(size = 7),  # Adjust text size as needed
  axis.title = element_text(size = 5),  # Adjust axis title size
  axis.text = element_text(size = 5, angle = 30, hjust = 1),  # Adjust axis text size
  legend.text = element_text(size = 5),  # Adjust legend text size
  legend.title = element_text(size = 5)  # Adjust legend title size
)
for (parameter in parameters_to_plot) {
  #print(parameter)
  parameter_summary_list <- lapply(model_names, extract_summary, parameter = parameter)
  parameter_summary_df <- do.call(rbind, parameter_summary_list)
  #print(factor(parameter))
  parameter_summary_df$parameter <- factor(parameter)
  
  transformed_df <- parameter_summary_df %>%
    mutate_if(is.numeric, reciprocal, parameter = parameter)
  #Plot posterior densities
  density_plot <- ggplot(transformed_df, aes(x = row.names(transformed_df), y = mean)) +
    geom_point() +
    geom_errorbar(aes(x = row.names(transformed_df), ymin = `0.025quant`, ymax = `0.975quant`), width = 0.2) +
    facet_wrap(~parameter, scales = "free_x") +
    labs(x = "", y = "Variance when precision") +
    theme_bw()+
    theme_options
  # , title = paste("Posterior Distribution for", parameter)
  # # Print plot
  #print(density_plot)
  # Store plot in the list
  plot_list[[parameter]] <- density_plot
}
# Arrange plots into multiple rows
combined_plots <- plot_grid(plotlist = plot_list, ncol = 4)

# Print the combined plot
print(combined_plots)

name="D:/OneDrive/tesis/CARPETA FINAL/reunion Pablo G/tesis_escrita/manuscrito2/hyperparametros_modelcomp.jpg"
ggsave(name)
```
# plot for the fixed effects accordinG to model agrupados por trat, amb, y trat*amb, segun modelo
```{r}

extract_summary_fix <- function(model) {
  marginals_hyperpar <- as.data.frame(get(model)$summary.fixed) %>% 
    mutate(modelo = model,names= row.names(get(model)$summary.fixed)) 
  }

fixed <- lapply(model_names, extract_summary_fix)

model_summary_df <- do.call(rbind, fixed)

#efectos<-unique(model_summary_df$names)

theme_options <- theme(
  #text = element_text(size = 7),  # Adjust text size as needed
  #axis.title = element_text(size = 5),  # Adjust axis title size
  axis.text = element_text(size = 6, angle = 45, hjust = 1)  # Adjust axis text size
  #legend.text = element_text(size = 5),  # Adjust legend text size
  #legend.title = element_text(size = 5)  # Adjust legend title size
)
density_plot_sum <- ggplot(model_summary_df, aes(x = names, y = mean, color = modelo)) +
      geom_point(position = position_dodge(width = 0.8)) +
      geom_errorbar(aes(x = names, ymin = `0.025quant`, ymax = `0.975quant`), width = 0.2, position = position_dodge(width = 0.8)) +
      #facet_wrap(~names, scales = "free_x") +
      labs(x = "", y = "", title = "Posterior Distribution for fixed effects") +
      theme_bw()+
      theme_options
    
print(density_plot_sum)

name="D:/OneDrive/tesis/CARPETA FINAL/reunion Pablo G/tesis_escrita/manuscrito2/posteriorDistri_modelcomp.jpg"
ggsave(name)

```

#segundo test gaussian plots
```{r}

## ! ESTO ESTA MAL, suponge una distribucion, la distribucion real esta en los marginals. x e y.
## Selle, compara para parametros elegidos, las campanas, teniendo Nospatial como base.
library(ggplot2)

# Function to plot Gaussian distribution for each fixed effect within a model
plot_gaussian <- function(data) {
  
  # Categorize unique values in the "names" column
  categorize_names <- function(names) {
    ifelse(grepl("^AMB", names), "AMB",
           ifelse(grepl(":", names), "Colon",
                  "Other"))
  }
  
  plot_data <- lapply(1:nrow(data), function(i) {
    if (data$names[i] != "(Intercept)") {  # Exclude "Intercept" effect
      x <- seq(data$`0.025quant`[i], data$`0.975quant`[i], length.out = 100)
      y <- dnorm(x, data$mean[i], data$sd[i])
      data.frame(x = x, y = y, names = data$names[i])
    } else {
      NULL  # Skip "Intercept" effect
    }
  })
  
  plot_data <- do.call(rbind, Filter(Negate(is.null), plot_data))  # Remove NULL elements
 
  
  ggplot(plot_data, aes(x = x, y = y, colour = names, linetype = categorize_names(names))) +
    geom_line() +
    #geom_point(aes(shape = categorize_names(names)), size = 1) +  # Set color based on fixed effect names
    labs(title = paste("Gaussian Distributions for Model", unique(data$model)[1]),
         x = "Value",
         y = "Density") +
    theme_bw() +
    theme(plot.title = element_text(hjust = 0.5),
          legend.position = "bottom"#,
          #legend.text = element_text(size = 5)#,  # Set legend text size
          #legend.key.size = unit(0.5, "cm")
          )+  # Set legend key size
    scale_linetype_manual(values = c("dashed", "dotted", "solid"), 
                          breaks = c("AMB", "Colon", "Other"), 
                          labels = c("AMB.", "AMBxTRAT", "TRAT."),
                          name = "Line Type") +  # Set legend title
    scale_color_discrete(name = "Effect")  # Set legend title
}


# Plot Gaussian distribution for each fixed effect within each model
plots <- lapply(split(model_summary_df, model_summary_df$model), plot_gaussian)

# Print each plot
for (plot in plots) {
  print(plot)
}

```

```{r}
modelos= modelos
variance_posterior_mean <- c(0.05, 0.03, 0.04)     # Mean variance posterior distribution for each 


```

#Graficas campanas varias
```{r}
#plot(fit_stefa$marginals.fixed$TRATAMIENTO, main = "Posterior fixed effects", type ='o')
#abline(v = beta)
plot(fit_stefa$marginals.fixed$`(Intercept)`, main = "Posterior intercept", type ='o')
#abline(v = PhenoData$Intercept[1])

##########aca solo funciona con modelo SPDE fit4 (u)
# Compute a SPDE result object with direct access to estimated range and spatial variance
#Fit2 = inla.spde.result(Fit, name = "Field", SpdeStat) 
plot(FitSTSPDE.2$marginals.range.nominal$range.nominal.1, main = "Posterior range", type ="o")
#abline(v = range0)
plot(FitSTSPDE.2$marginals.variance.nominal$variance.nominal.1, main = "Posterior spatial variance", type = "o")
#abline(v = varE*(errorSpatial))
##########

#error residual (v).
plot(inla.tmarginal(function(x) 1/x, fit_stefa$marginals.hyperpar$`Precision for the Gaussian observations`), type = 'o', main = "Posterior residual variance")

```


