# article title: Modelling spatial variation in agricultural field trials with INLA
# journal name: Theoretical and Applied Genetics
# authors: 
# Maria Lie Selle, maria.selle@ntnu.no, Department of Mathematical Sciences, Norwegian University of Science and Technology (NTNU), Trondheim, Norway   
# Ingelin Steinsland 
# John M. Hickey    
# Gregor Gorjanc 



# Online resource 1: Fitting nelder wheel plots using the SPDE model and INLA

# In this script we show how to 
    # - set up a Nelder wheel design
    # - simulate a Gaussian random field with the SPDE approach
    # - perform all steps to fit a model with the SPDE approach
    #      - build mesh
    #      - construct matrix A that maps between the mesh and plot locations
    #      - specify formula
    #      - make the SPDE object and corresponding index
    #      - stack data, effects and projection matrix A
    #      - call inla function
    # - examine the result from inla


# ---- Define function that simulates from a SPDE model ----

SimulateSpdeField = function(mesh, n.fields=1, range0, sigma0) {
  # mesh: an inla mesh
  # n.fields: how many separate fields
  # range0: number
  # sigma0: number
  
  # output: a vector containing n.fields sample fields from the SPDE model
  
  # Compute values from known range and variance
  kappa0 = sqrt(8) / range0
  tau0 = 1 / (sqrt(4 * pi) * kappa0 * sigma0)
  
  # Define the spde object
  spde.stat = inla.spde2.matern(mesh = mesh,
                                B.tau = matrix(c(0, 1, 0), nrow = 1, ncol = 3),
                                B.kappa=matrix(c(0, 0, 1), nrow = 1, ncol = 3))
  
  # Compute the precision matrix of the spde object
  Q.stat = inla.spde2.precision(spde = spde.stat,
                                theta = c(log(tau0), log(kappa0)))
  
  # Draw sample
  sample.field = as.vector(inla.qsample(n = n.fields, Q = Q.stat))
  
  return(sample.field)
}

# ---- Load INLA package ----

# INLA must be downloaded separately using the call:
# install.packages("INLA", repos=c(getOption("repos"), INLA="https://inla.r-inla-download.org/R/stable"), dep=TRUE)
# or go to http://www.r-inla.org/download
library(INLA)

# ---- Set tree locations and design field layout ----

r0 = 10 # Radius of first ring
k = 1.15 # Radius increase factor
t1 = 10 # Number of densities to test
t2 = 30 # Number of trees per density

w0 = 2*pi/t2
nTrees = t1*t2

# Radius and angle
r = sapply(0:(t1-1), function(x) r0*k^x)
w = sapply(0:(t2-1), function(x) w0 + w0*x)

# Locations
LocationsPolar = expand.grid(r = r, w = w)
LocationsCart = cbind(LocationsPolar$r * cos(LocationsPolar$w), 
                      LocationsPolar$r * sin(LocationsPolar$w) )

# Plot the design
par(mfrow = c(1,1))
par(mar=c(4.1,4.1,2.1,2.1))
plot(LocationsCart[,1], LocationsCart[,2], ylab = "Northing", xlab = "Easting",
     lwd = 1, cex.lab = 1.5, cex.axis = 1.5)

# Area and density
Area = rep(w0,t1*t2) * LocationsPolar$r ^2 *(k-k^(-1))/2
Density = Area^(-1)

# ---- Simulate density effect ----

PhenoData = data.frame(individualID = 1:nTrees)

# We simulate a density effect
PhenoData$Density = Density
beta = 10
PhenoData$DensityEff = beta*PhenoData$Density

# ---- Construct mesh and simulate spatial field ----

# The mesh represents the spatial process and must cover the enitre spatial domain of interest
mesh.cutoff = c(3) # Minimum edge length
mesh.max.edge = c(6, 12) # Maximum edge length in inner and outer area
offset = c(15) # Distance to outer area

a = seq(from = 10, to = 300, length.out = 30) # Pick out the locations of outer ring to use for the outer region of out spatial domain of interest
Mesh = inla.mesh.2d(loc = LocationsCart[a,],  max.edge = mesh.max.edge, 
                    cutoff = mesh.cutoff, offset = offset)
par(mfrow = c(1,1))
plot(Mesh, main = NULL)
points(LocationsCart[,1], LocationsCart[,2], col = "red", pch = 20, lwd = 0.5)

# Now that we have the mesh, we can simulate a spatial field
varE = 1 
errorSpatial = 0.5 # How much of varE should go into the spatial effect. The rest of varE will be the variance of the error term
range0 = 20
# The spatial field
SampleField = SimulateSpdeField(mesh = Mesh, n.fields = 1,
                                range0 = range0, sigma0 = sqrt(varE * errorSpatial))

# Plot underlying spatial field
proj = inla.mesh.projector(Mesh,dims = c(300, 300))
field.proj = inla.mesh.project(proj, SampleField)
#install.packages("fields")
library(fields)
image.plot(list(x = proj$x, y = proj$y, z = field.proj), col = heat.colors(12))
points(LocationsCart)

# The matrix A maps the sampled field effects from the mesh nodes to the plot locations 
# and we add the true spatial effects to the data frame PhenoData
A = inla.spde.make.A(mesh = Mesh, loc = as.matrix(LocationsCart))
PhenoData$PlotEff = as.vector(A %*% SampleField) 

PhenoData$xLoc = LocationsCart[,1]
PhenoData$yLoc = LocationsCart[,2]

# ---- Sample random noise, set intercept and make the observed phenotype  ----

PhenoData$Intercept = 10
PhenoData$ErrorEff = rnorm(n = nTrees, mean = 0, sd = sqrt(varE * (1 - errorSpatial)))

PhenoData$Phenotype = PhenoData$DensityEff + PhenoData$Intercept + PhenoData$PlotEff + PhenoData$ErrorEff

# ---- Fit model to the data ----

# Make the formula
Formula = Phenotype ~ Intercept + Density + f(Field, model = SpdeStat) - 1

# Define the SPDE model that we have specified in the formula 
# We use the mesh from when simulating data. If we dont simulate data, we must construct a mesh here  
SpdeStat = inla.spde2.matern(mesh = Mesh, alpha = 2)
MeshIndex = inla.spde.make.index(name = "Field", n.spde = SpdeStat$n.spde) 
# The name here must be the same as the first argument in the f() function in the formula

# The observed data and effects must be stored in a stack object together with matrices that define the mapping between them. 
# The intercept and spatial effect are mapped to the observations through the projector matrix A,
# and the density effect has a one-to-one map of the covariate and the response. 
# The latter matrix can be a constant rather than a diagonal matrix.
Stack = inla.stack(data = list(Phenotype = PhenoData$Phenotype),
                   A = list(A, 1),
                   effects = list(c(MeshIndex, list(Intercept = 1)),
                                  c(list(Density = PhenoData$Density))), tag = "est") 
# We use the A-matrix from when simulating data. If we dont simulate data, we must compute the A matrix before making the stack

# Now we are ready to estimate all effects using inla. 
Fit = inla(formula = Formula,
           data = inla.stack.data(Stack, spde = SpdeStat),
           control.predictor = list(A = inla.stack.A(Stack)))
summary(Fit)

# --- Plot the posterior distributions for fixed effects and hyperparameters ----

plot(Fit$marginals.fixed$Density, main = "Posterior density effect", type ='o')
abline(v = beta)
plot(Fit$marginals.fixed$Intercept, main = "Posterior intercept", type ='o')
abline(v = PhenoData$Intercept[1])

# Compute a SPDE result object with direct access to estimated range and spatial variance
Fit2 = inla.spde.result(Fit, name = "Field", SpdeStat) 
plot(Fit2$marginals.range.nominal$range.nominal.1, main = "Posterior range", type ="o")
abline(v = range0)
plot(Fit2$marginals.variance.nominal$variance.nominal.1, main = "Posterior spatial variance", type = "o")
abline(v = varE*(errorSpatial))

plot(inla.tmarginal(function(x) 1/x, Fit$marginals.hyperpar$`Precision for the Gaussian observations`), 
     type = 'o', main = "Posterior residual variance")
abline(v = varE * (1 - errorSpatial))

# ---- Plot the true field, the mean estimated field, and the standard deviation of the field ----

par(mfrow = c(1, 3))
par(mar = c(4.1, 5.1, 2.1, 2.1))
proj = inla.mesh.projector(Mesh, dims = c(300, 300))
# True field

field.proj.true = inla.mesh.project(proj, SampleField)
image.plot(list(x = proj$x, y=proj$y, z = field.proj.true), col = heat.colors(12),
           ylim = c(-40, 40),xlim = c(-40, 40), zlim = c(min(field.proj.true, na.rm = TRUE),
                                                         max(field.proj.true, na.rm = TRUE)),
           main = NULL, xlab = "Easting", ylab = "Northing")
points(LocationsCart)

# Estimated field mean
field.proj = inla.mesh.project(proj, Fit$summary.random$Field$mean)
image.plot(list(x = proj$x, y=proj$y, z = (field.proj)), col = heat.colors(10),
           ylim = c(-40, 40),xlim = c(-40, 40), zlim = c(min(field.proj.true, na.rm = TRUE), 
                                                         max(field.proj.true, na.rm = TRUE)),
           main = NULL, xlab = "Easting", ylab = "Northing")
points(LocationsCart)

# Estimated field sd
field.proj = inla.mesh.project(proj, Fit$summary.random$Field$sd)
image.plot(list(x = proj$x, y = proj$y, z = (field.proj)), col = heat.colors(10),
           ylim = c(-40, 40), xlim = c(-40, 40),
           main = NULL, xlab = "Easting", ylab = "Northing")
points(LocationsCart)

